# Feeds in OPML format

Build your own, cheap and valuable newspaper!

## Intro

This repository tries to solve the information metaproblem about where to
get information? Not any information, but meaningful, i.e, which helps us
act and make us think. Where to reach for news?

This collection of OPML files is sorted by topic. As any collection, it's
contents were selected using some arbitrary criteria and hence might not
be of universal acceptance.

So feel free to propose different groupings, submit pull requests or even
forking this repository.

And don't forget about that:

    The news’s obsession with having a little bit of information on a wide
    variety of subjects means that it actually gets most of those subjects wrong.

    -- Aaron Swartz,
       http://www.aaronsw.com/weblog/hatethenews
       https://news.ycombinator.com/item?id=9224858
       Accessed on 2024-09-03

Also watchout for the [Fake news phenomenon](https://en.wikipedia.org/wiki/Fake_news).

And always prefer a good book, essay, paper or analysis instead of fragmented,
uncontextualized information!

## How to use

Simply import the OPML files into your feed readers. In newsbeuter, you
can simply run the following command for each OPML file:

    newsbeuter -i /path/to/feeds/topic.opml

See http://newsbeuter.org/doc/newsbeuter.html#_migrating_from_other_rss_feed_readers
for more information.

Some feed readers even support to read directly from the OPML files. In newsbeuter,
this is achieved with just two lines in the configuration:

    urls-source "opml"
    opml-url "file:///path/to/feeds/topic1.opml" "file:///path/to/feeds/topic2.opml"

See http://newsbeuter.org/doc/newsbeuter.html#_opml_online_subscription_mode
for more information.

You can even point to the latest OPML version which should be available through
the canonical URL:

    opml-url "https://git.fluxo.info/feeds/plain/topic.opml"

## How to make your own

Copy `sample.opml` to a new file and edit to include your own set of feeds!

## Development

Use the [compile](compile) script to build an OPML including all other
OPML files.

## References

* For the OPML spec, check http://dev.opml.org
